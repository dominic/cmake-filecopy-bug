#!/bin/bash

mkdir build
cd build
cmake .. > firstrun.log
echo "Here, the copied file seems to be younger than the original, because the time stamp was rounded down:"
ls --full tmp/example.txt
ls --full example.txt
cd ..
rm -rf build
mkdir build
cd build
cmake -DSECOND_RUN=1 .. > secondrun.log
echo "Executing CMake a second time, appending to the output and copying again, the second write gets lost:"
echo "Original:"
cat tmp/example.txt
echo "Copy:"
cat example.txt
